class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get getName() {
    return this.name;
  }
  get getAge() {
    return this.age;
  }
  get getSalary() {
    return this.salary;
  }
  set setAge(value) {
    this.age = value;
  }
  set setName(value) {
    this.name = value;
  }
  set setSalary(value) {
    this.salary = value;
  }
}

const employee = new Employee("Ramin", "40", "4000");
console.log(employee);

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get getSalary() {
    return this.salary * 3;
  }
}
const programmer = new Programmer("Roma", "35", "1000", "eng");
const designer = new Programmer("Anton", "22", "600", "ukr");
const police = new Programmer("Jeremy", "35", "3400", "esp");

console.log(programmer);
console.log(designer);
console.log(police);

console.log(programmer.getSalary);
console.log(designer.getSalary);
console.log(police.getSalary);
